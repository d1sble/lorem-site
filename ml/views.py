from django.shortcuts import render
from .models import Sentence

# Create your views here.
def form(request):
    sentences = Sentence.objects.order_by('-created_at').first()
    return render(request, 'ml/form.html', {'sentences':sentences})

