from django.db import models
from django.utils import timezone

# Create your models here.
class Sentence(models.Model):
    sentence = models.CharField(max_length=255, default=None)
    is_good = models.BooleanField(default=None, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    
    class Meta():
        verbose_name = 'Sentence'
        verbose_name_plural = 'Sentences'

    def __str__(self) -> str:
        return self.sentence