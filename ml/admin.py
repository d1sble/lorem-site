from django.contrib import admin
from .models import Sentence



class SentenceAdmin(admin.ModelAdmin):
    list_display = ('sentence', 'is_good', 'created_at')

# Register your models here.
admin.site.site_title = "Admin page"
admin.site.site_header = "Administration"
admin.site.index_title = "admin panel"

admin.site.register(Sentence, SentenceAdmin)

